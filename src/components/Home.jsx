import { useEffect } from 'react'
import "./Home.css"

export default function Home() {

useEffect(() => {
 if(typeof window != undefined){
  let iframe = window.document.getElementById("myIframe")
  console.log(iframe)
 }
  
}, [])

  

  return (
    <div className='home_container'>
      <div className='home_recording'>recording</div>
      <iframe id="myIframe" src="https://facebook.com/" width="100%" height="100%"></iframe>
    </div>
  )
}
